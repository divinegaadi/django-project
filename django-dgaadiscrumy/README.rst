dgaadiscrumy is a simple django app



Quickstart
1. add "dgaadiscrumy to your INSTALLED_APPS setting like this:
    INSTALLED_APPS = [
        ...'dgaadiscrumy'
    ]

2. insludethe dgaadiscrumy in your project urls.py like this:
    path('dgaadiscrumy/', inslude('dgaadiscrumy.urls'))

3. Run `python manage.py migrate` to create dgaadiscrumys models.

4. start the development server and visit http://127.0.0.1:8000/admin/
    to create an admin (you'll need the admin app enabld).

5. visit http://127.0.0.1:8000/dgaadiscrumy/ to show the add