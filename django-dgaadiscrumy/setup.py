import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:

    os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='django-dgaadiscrumy',
    version='0.1',
    packages=find_packages(),
    iclude_package_date=True,
    license='BSD License',
    description='A simple django app.',
    long_description='README',
    url='https://www/example.com/',
    author='divine gaadi',
    author_email='dgaadi@gmail.com',
    classifiers=[
        'Environment :: Web Environment',
        'Freamework :: Django',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)