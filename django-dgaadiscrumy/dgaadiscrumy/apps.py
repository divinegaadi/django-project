from django.apps import AppConfig


class DgaadiscrumyConfig(AppConfig):
    name = 'dgaadiscrumy'
